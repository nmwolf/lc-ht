import sys
import xml.etree.cElementTree as et
import gzip
import re
import sqlite3
from multiprocessing import Pool
import time

files_process = str(sys.argv[1]).split(',')

#conn = sqlite3.connect("/scratch/nmw2/lc-ht/lc-ht-oclc.db", timeout=10)
conn = sqlite3.connect("/Users/nmw2/Desktop/lc-ht/lc-ht.db", timeout=10)
cursor = conn.cursor()
cursor.execute("""CREATE TABLE IF NOT EXISTS lcrecords
                  (lc_num text not null unique, file_num text not null, marc_035 text, oclc_num text, author_lname text,
                   author_fname text, year_pub text, title_pub text, place_pub text)
               """)

#directory = r'/scratch/nmw2/lc-ht-project/lc-files/'
directory = r'/Volumes/nmw2/main/lc-files/'

def loc_extractor(flines):
    recstarts = [i for i, s in enumerate(flines) if re.search(r'<record>', s)]
    recstops = [i for i, s in enumerate(flines) if re.search(r'</record>', s)]
    return list(zip(recstarts, recstops))


def parse_xml(ct):
    with gzip.open(directory + 'BooksAll.2014.part' + ct + '.xml.gz', 'rt') as f:
        print("Opening file... ", ct)
        fulltree = f.readlines()
        print("Commencing work on file ", str(ct), ". Number of lines: ", str(len(fulltree)))
        rec_locs = loc_extractor(fulltree)
        f.close()
        print("There will be ", len(rec_locs), " records in this batch: ", ct)
        for loc in rec_locs:
            for field in et.fromstring(''.join([i.replace('\n','') for i in fulltree[loc[0]:(loc[1]+1)]])):
                if field.tag == 'controlfield' and field.attrib.get('tag') == '001':
                    num = field.text.strip()
                if field.tag == 'datafield':
                    if field.attrib.get('tag') == '035':
                        try:
                            if field[0].attrib.get('code') == 'a':
                                marc35 = field[0].text.strip()
                                if re.search(r'OCoLC', field[0].text, re.I):
                                    oclc = field[0].text.replace('(OCoLC)', '').strip()
                        except:
                            oclc = 'NONE'
                    if field.attrib.get('tag') == '100':
                        if field[0].attrib.get('code') == 'a':
                            try:
                                lname, fname = field[0].text.split(',')[0].strip(), ' '.join(field[0].text.split(',')[1:]).strip()
                            except:
                                try:
                                    lname = field[0].text.strip()
                                except:
                                    lname, fname = "NONE", "NONE"
                    if field.attrib.get('tag') == '245':
                        try:
                            if field[0].attrib.get('code') == 'a':
                                title = field[0].text.strip()
                            if field[1].attrib.get('code') == 'b':
                                title = title + ' ' + field[1].text.strip()
                        except:
                            pass
                    if field.attrib.get('tag') == '260':
                        try:
                            if field[0].attrib.get('code') == 'a':
                                place = field[0].text.replace(',','').strip()
                            if field[2].attrib.get('code') == 'c':
                                year = field[2].text.replace(',','').replace('.','').strip()
                        except:
                            pass

            attempts = 0
            while attempts < 10:
                try:
                    cursor.execute("INSERT INTO lcrecords VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",(num, ct, marc35, oclc, lname, fname,
                                                                               year, title, place))
                    attempts = 10
                except UnboundLocalError:
                    try:
                        marc35
                    except:
                        marc35 = "NONE"
                    try:
                        oclc
                    except NameError:
                        oclc = "NONE"
                    try:
                        lname
                    except NameError:
                        lname = "NONE"
                    try:
                        fname
                    except NameError:
                        fname = "NONE"
                    try:
                        year
                    except NameError:
                        year = "NONE"
                    try:
                        title
                    except NameError:
                        title = "NONE"
                    try:
                        place
                    except NameError:
                        place = "NONE"
                except sqlite3.IntegrityError:
                    attempts = 10
                except sqlite3.OperationalError:
                    time.sleep(5)
                    attempts+=1
            conn.commit()
            del num
            del marc35
            del oclc
            del lname
            del fname
            del title
            del year
            del place
    print("Completed file ", ct)

pool = Pool()


for fct in pool.imap(parse_xml, files_process):
    pass
