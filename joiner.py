import re
import sqlite3
from multiprocessing import Pool
import time
import pandas as pd
from IPython.display import display
from fuzzywuzzy import fuzz
import numpy as np

directory = r'/Users/nmw2/Desktop/lc-ht/'
start = time.clock()

def loc_connect():
    conn = sqlite3.connect(directory + '/lc-ht.db', timeout=10)
    lc_df = pd.read_sql_query("SELECT * FROM lcrecords;", conn)
    conn.close()
    return lc_df

ht_df_1 = pd.DataFrame(np.load(directory + 'unique_ht_oclc_df_0.npy'))
ht_df_2 = pd.DataFrame(np.load(directory + 'unique_ht_oclc_df_4000000.npy'))
ht_df_3 = pd.DataFrame(np.load(directory + 'unique_ht_oclc_df_8000000.npy'))
ht_df_4 = pd.DataFrame(np.load(directory + 'unique_ht_oclc_df_12000000.npy'))
ht_df_5 = pd.DataFrame(np.load(directory + 'unique_ht_oclc_df_16000000.npy'))
unique_ht_oclc = pd.concat([ht_df_1, ht_df_2, ht_df_3, ht_df_4, ht_df_5], ignore_index=True)
unique_ht_oclc.columns = ['oclc']
lc_df = loc_connect()
print(len(lc_df))
print(len(unique_ht_oclc))


## Light clean up of some values in the LC dataframe

lc_df['oclc_num'] = lc_df['oclc_num'].str.replace('ocm', '', flags=re.I)
lc_df['oclc_num'] = lc_df['oclc_num'].str.replace('ocn', '', flags=re.I)
lc_df['oclc_num'] = lc_df['oclc_num'].str.replace('(OColC)', '', flags=re.I)
lc_df['oclc_num'] = lc_df['oclc_num'].str.replace('\s', '')
lc_df['oclc_num'] = lc_df['oclc_num'].str.replace('n', '')
lc_df['oclc_num'] = lc_df['oclc_num'].str.replace('(', '')
lc_df['oclc_num'] = lc_df['oclc_num'].str.replace(')', '')

lc_df['year_pub'] = lc_df['year_pub'].str.replace('l','1')

## JOINS


# We do an inner join (intersection; i.e. found in both LoC and HT) on OCLC numbers in both LC and HT
# Resulting data frame gives us the LoC numbers (lc_num) of any records from LoC in Hathi

lc_oclc_match_df = pd.merge(lc_df, unique_ht_oclc, how='inner', left_on='oclc_num', right_on='oclc')
print("A join of unique oclc numbers from Hathi with LoC yields ", len(lc_oclc_match_df), " matches.")
lc_oclc_match_df['match_type'] = "oclc"

# We can use that list to remove rows from LoC dataframe that we don't need to investigate further

unmatched_lc_df = lc_df[~lc_df.lc_num.isin(lc_oclc_match_df.lc_num.tolist())]
print("This leaves ", len(unmatched_lc_df), " LoC records that have not been matched.")


## Loading and Merging HT Index Files

i_0 = np.load(directory + 'yearIndex_0.npy').item()
i_1 = np.load(directory + 'yearIndex_4000000.npy').item()
i_2 = np.load(directory + 'yearIndex_8000000.npy').item()
i_3 = np.load(directory + 'yearIndex_12000000.npy').item()
i_4 = np.load(directory + 'yearIndex_16000000.npy').item()

year_ind = {}

for ind in [i_0, i_1, i_2, i_3, i_4]:
	for year in ind:
		if year in year_ind:
			for title_word in ind[year]:
				if title_word in year_ind[year]:
					for title_pair in ind[year][title_word]:
						year_ind[year][title_word].append(title_pair)
				else:
					year_ind[year][title_word] = []
					for title_pair in ind[year][title_word]:
						year_ind[year][title_word].append(title_pair)
		else:
			year_ind[year] = {}
			for title_word in ind[year]:
				if title_word in year_ind[year]:
					for title_pair in ind[year][title_word]:
						year_ind[year][title_word].append(title_pair)
				else:
					year_ind[year][title_word] = []
					for title_pair in ind[year][title_word]:
						year_ind[year][title_word].append(title_pair)

print("A final merged index of ", len(year_ind), " was created. Starting match...")

## Matching using the constructed index

matchcount = 0
matchpairs = []

for lc_rec in unmatched_lc_df.itertuples():
    try:
        lc_year = re.findall(r'\d\d\d\d', lc_rec[7])[0]
        lc_title_one = re.sub(r'\W+', '', lc_rec[8].split()[0])
        try:
            lc_title_summary = ' '.join(lc_rec[8].split()[0:8]) if len(lc_rec[8].split()) >= 8 else None  # We set a minimum of 8 words in LC title to match
            if lc_title_summary:
                try:
                    cands = year_ind[lc_year][lc_title_one]
                    for cand in cands:
                        try:
                            ht_title_words = ' '.join(cand[0].split()[0:8]) if len(cand[0].split()) >= 8 else None  # Setting the same minimum for HT title
                            if ht_title_words:
                                if fuzz.ratio(ht_title_words, lc_title_summary) > 97:
                                    matchcount+=1
                                    matchpairs.append((lc_rec[1], cand[1], lc_title_summary, ht_title_words))
                                    break
                            else:
                                continue   #Fails because insufficient number of tokens in HT title match candidate
                        except:
                            pass  #Fails because no HT title field value
                except:
                    continue    #Fails because year + first title word not in index
            else:
                continue #Fails because insufficient number of tokens in LC title
        except:
            continue  #Fails because nothing in title field
    except:
        continue  #Fails because no year in year_pub field
        
        
print(matchcount, " matched.")
np.save(directory + "index_match_pairs.npy", matchpairs)

end = time.clock()
print("Time elapsed = ", (end-start)/3600)
