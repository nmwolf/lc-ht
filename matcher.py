import re
import sqlite3
from multiprocessing import Pool
import time
import pandas as pd
from IPython.display import display
from fuzzywuzzy import fuzz

directory = r'/Volumes/GoogleDrive/My Drive/py/data/lc-ht/'

def loc_connect():
    conn = sqlite3.connect(directory + "lc-ht.db", timeout=10)
    lc_df = pd.read_sql_query("SELECT * FROM lcrecords limit 3000000;", conn)
    conn.close()
    return lc_df

def ht_connect():
    conn = sqlite3.connect(directory + "hathi.db", timeout=10)
    ht_df = pd.read_sql_query("""SELECT ht_id, access, source, oclc, title_245,
                              pubdate FROM ht limit 3000000;""", conn)
    conn.close()
    return ht_df

lc_df = loc_connect()
ht_df = ht_connect()
print(len(lc_df))
print(len(ht_df))

## Light clean up of some values in the LC dataframe

lc_df['oclc_num'] = lc_df['oclc_num'].str.replace('ocm', '', flags=re.I)
lc_df['oclc_num'] = lc_df['oclc_num'].str.replace('ocn', '', flags=re.I)
lc_df['oclc_num'] = lc_df['oclc_num'].str.replace('(OColC)', '', flags=re.I)
lc_df['oclc_num'] = lc_df['oclc_num'].str.replace('\s', '')
lc_df['oclc_num'] = lc_df['oclc_num'].str.replace('n', '')
lc_df['oclc_num'] = lc_df['oclc_num'].str.replace('(', '')
lc_df['oclc_num'] = lc_df['oclc_num'].str.replace(')', '')

lc_df['year_pub'] = lc_df['year_pub'].str.replace('l','1')

## Building out the list of additional rows needed to account for multiple OCLC numbers in each HT record under OCLC field

original_length = len(ht_df)
df_index = 0
append_rows = []
null_oclc_ht_count = 0

while df_index < len(ht_df):
    try:
        if re.search(r',', ht_df.iloc[df_index,3]):
            append_rows.append((ht_df.iloc[df_index].copy(),ht_df.iloc[df_index,3].split(',')[1:]))
            ht_df.iloc[df_index,3] = ht_df.iloc[df_index,3].split(',')[0]
    except:
        null_oclc_ht_count += 1
    df_index+=1

print("Number of appended rows due to multiple OCLC nums: ", len(append_rows))
print("Number of oclc values that were null: ", null_oclc_ht_count)

## Constructing an append DF based on those additional rows needed

append_df = pd.DataFrame()
for f in append_rows:
    for extra_oclc in f[1]: 
        app_row = f[0].copy()
        app_row[3] = extra_oclc
        append_df = append_df.append(app_row, ignore_index=True)
    if len(append_df)%20000 == 0:
        print(len(append_df))
        
        
ht_df = ht_df.append(append_df, ignore_index=True)

print("The Hathi DF has increased by ", len(ht_df)-original_length, " rows.")

## JOINS

# First we create a simple dataframe containging all unique oclc numbers in the Hathifiles

unique_ht_oclc = pd.DataFrame(ht_df.oclc.unique(), columns=['oclc'])

# Then we do an inner join (intersection; i.e. found in both LoC and HT) on OCLC numbers in both LC and HT
# Resulting data frame gives us the LoC numbers (lc_num) of any records from LoC in Hathi

lc_oclc_match_df = pd.merge(lc_df, unique_ht_oclc, how='inner', left_on='oclc_num', right_on='oclc')
print("A join of unique oclc numbers from Hathi with LoC yields ", len(lc_oclc_match_df), " matches.")

# We can use that list to remove rows from LoC dataframe that we don't need to investigate further

unmatched_lc_df = lc_df[~lc_df.lc_num.isin(lc_oclc_match_df.lc_num.tolist())]
print("This leaves ", len(unmatched_lc_df), " LoC records that have not been matched.")

# We can do the same for the HathiFiles:

unmatched_ht_df = ht_df[~ht_df.oclc.isin(lc_oclc_match_df.oclc.tolist())]
print("With ", len(unmatched_ht_df), " potential Hathi Files to match against.")

## INDEX BUILDING FOR UNMATCHED RECORDS

year_ind = {}
first_ind = {}
second_ind = {}
notitle_count = 0

for rec in unmatched_ht_df.itertuples():
    if rec[6] == '':
        notitle_count+=1
        continue
    try:
        first_word = re.sub(r'\W+', '', rec[6].split()[0])
    except:
        continue
    try:
        second_word = re.sub(r'\W+', '', rec[6].split()[1])
    except IndexError:
        second_word = None
    try:
        first_ind[first_word].append([rec[6],rec[2]])
    except KeyError:
        first_ind[first_word] = []
        first_ind[first_word].append([rec[6],rec[2]])
    if second_word:
        try:
            second_ind[second_word].append([rec[6],rec[2]])
        except KeyError:
            second_ind[second_word] = []
            second_ind[second_word].append([rec[6],rec[2]])
    else:
        pass
    yr_ind = None
    try:
        yr_ind = re.findall(r'\d\d\d\d', rec[4])[0]
    except:
        pass
    if yr_ind != None:
        try:
            year_ind[yr_ind]
            try:
                year_ind[yr_ind][first_word].append([rec[6],rec[2]])
            except:
                year_ind[yr_ind][first_word] = []
                year_ind[yr_ind][first_word].append([rec[6],rec[2]])
        except:
            year_ind[yr_ind] = {}
            try:
                year_ind[yr_ind][first_word].append([rec[6],rec[2]])
            except:
                year_ind[yr_ind][first_word] = []
                year_ind[yr_ind][first_word].append([rec[6],rec[2]])
    del yr_ind
    del first_word
    del second_word
            
print("A total of ", notitle_count, " records did not have any title.")
print("An index of years with this many records: ",len(year_ind))
print("An index of first words with this many records: ",len(first_ind))

## Matching using the constructed index

matchcount = 0
matchpairs = []

for lc_rec in unmatched_lc_df.itertuples():
    try:
        lc_year = re.findall(r'\d\d\d\d', lc_rec[7])[0]
        lc_title_one = re.sub(r'\W+', '', lc_rec[8].split()[0])
        try:
            lc_title_summary = ' '.join(lc_rec[8].split()[0:8]) if len(lc_rec[8].split()) >= 8 else None  # We set a minimum of 8 words in LC title to match
            if lc_title_summary:
                try:
                    cands = year_ind[lc_year][lc_title_one]
                    for cand in cands:
                        try:
                            ht_title_words = ' '.join(cand[0].split()[0:8]) if len(cand[0].split()) >= 8 else None  # Setting the same minimum for HT title
                            if ht_title_words:
                                if fuzz.ratio(ht_title_words, lc_title_summary) > 97:
                                    matchcount+=1
                                    matchpairs.append((lc_rec[1], cand[1], lc_title_summary, ht_title_words))
                                    break
                            else:
                                continue   #Fails because insufficient number of tokens in HT title match candidate
                        except:
                            pass  #Fails because no HT title field value
                except:
                    continue    #Fails because year + first title word not in index
            else:
                continue #Fails because insufficient number of tokens in LC title
        except:
            continue  #Fails because nothing in title field
    except:
        continue  #Fails because no year in year_pub field
        
        
print(matchcount, " matched.")
print("An index of second words with this many records: ",len(second_ind))