import sys
import gzip
import sqlite3
import time

mil_start = int(sys.argv[1])

conn = sqlite3.connect("/Users/nmw2/Desktop/lc-ht/hathi.db", timeout=10)
cursor = conn.cursor()
cursor.execute("""CREATE TABLE IF NOT EXISTS ht
                  (ht_id text not null unique, access text, rights text, cid text,
                  enumchron text, source text, local_id text, oclc text, isbn text,
                  issn text, lccn text, title_245 text, imprint_260 text, rights_det text,
                  date_update text, gov_doc text, pubdate text, pubplace text, language text,
                  bib_format text, coll_code text, provider_code text, resp_entity text,
                  digitizing_agent text)
               """)

#directory = r'/Volumes/nmw2/main/lc-files/'
#directory = r'/Volumes/GoogleDrive/My Drive/py/'
directory = r'/Users/nmw2/Desktop/lc-ht/'

count = 0

with gzip.open(directory + '257828', 'rt') as f:
    rows = f.readlines()
    print("Commencing work on file. Number of lines: ", str(len(rows)))
    f.close()
    for row in rows[(mil_start*1000000):]:
        insert_row = row.split('\t')
        attempts = 0
        while attempts < 10:
            try:
                cursor.execute("INSERT INTO ht VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",insert_row)
                attempts = 10
            except sqlite3.IntegrityError:
                attempts = 10
            except sqlite3.OperationalError:
                time.sleep(5)
                attempts+=1
                if attempts > 10:
                    attempts = int(raw_input("Number of attempts exceeded 10. Reset and proceed? Enter 0"))
        conn.commit()
        count+=1
        if count%100000 == 0:
            print("Completed a 100000 recs")
print("Completed parse")
