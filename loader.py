import re
import sqlite3
from multiprocessing import Pool
import time
import pandas as pd
from IPython.display import display
from fuzzywuzzy import fuzz
import numpy as np

#directory = r'/Volumes/GoogleDrive/My Drive/py/data/lc-ht/'
directory = r'/scratch/nmw2/lc-ht/'


def ht_connect(lim, off):
    conn = sqlite3.connect(directory + "hathi.db", timeout=10)
    ht_df = pd.read_sql_query("""SELECT ht_id, access, source, oclc, title_245,
                              pubdate FROM ht limit %s offset %s;""" % (lim, off), conn)
    conn.close()
    return ht_df



def extract_ht_oclc(invals):
    start = time.clock()
    ht_df = ht_connect(invals[0],invals[1])
    print("HT DF of length ", len(ht_df), " rows for offset ", invals[1])

    ## Building out the list of additional rows needed to account for multiple OCLC numbers in each HT record under OCLC field

    original_length = len(ht_df)
    df_index = 0
    append_rows = []
    null_oclc_ht_count = 0

    while df_index < len(ht_df):
        try:
            if re.search(r',', ht_df.iloc[df_index,3]):
                append_rows.append((ht_df.iloc[df_index].copy(),ht_df.iloc[df_index,3].split(',')[1:]))
                ht_df.iloc[df_index,3] = ht_df.iloc[df_index,3].split(',')[0]
        except:
            null_oclc_ht_count += 1
        df_index+=1

    print("Number of appended rows due to multiple OCLC nums: ", len(append_rows), "for offset ", invals[1])
    print("Number of oclc values that were null: ", null_oclc_ht_count, "for offset ", invals[1])
    np.save(directory + 'append_dict_' + str(invals[1]) + '.npy', append_rows)

    ## Constructing an append DF based on those additional rows needed

    append_df = pd.DataFrame()
    for f in append_rows:
        for extra_oclc in f[1]: 
            app_row = f[0].copy()
            app_row[3] = extra_oclc
            append_df = append_df.append(app_row, ignore_index=True)
        if len(append_df)%20000 == 0:
            print(len(append_df))
        
        
    ht_df = ht_df.append(append_df, ignore_index=True)

    print("The Hathi DF has increased by ", len(ht_df)-original_length, " rows for offset ", invals[1])


    # We create a simple dataframe containging all unique oclc numbers in the Hathifiles

    unique_ht_oclc = pd.DataFrame(ht_df.oclc.unique(), columns=['oclc'])

    # We also want it as a list for saving:

    unique_ht_oclc_list = list(set(ht_df.oclc.tolist()))

    np.save(directory + 'unique_ht_oclc_list_' + str(invals[1]) + '.npy', unique_ht_oclc_list)
    np.save(directory + 'unique_ht_oclc_df_' + str(invals[1]) + '.npy', unique_ht_oclc)
    end = time.clock()
    print("Time elapsed = ", (end-start)/3600, "hours for offset ", invals[1])


pool = Pool(processes=5)

rgs = [(4000000,0),(4000000,4000000),(4000000,8000000),(4000000,12000000),(208265,16000000)]

pool.map(extract_ht_oclc,rgs)


