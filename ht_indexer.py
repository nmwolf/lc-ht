import re
import sqlite3
from multiprocessing import Pool
import time
import pandas as pd
import numpy as np


start = time.clock()


def ht_indexer(inval):
    directory = r'/Users/nmw2/Desktop/lc-ht/'
    conn = sqlite3.connect(directory + "hathi.db", timeout=10)
    ht_df = pd.read_sql_query("""SELECT ht_id, access, source, oclc, title_245,
                              pubdate FROM ht limit %s offset %s;""" % (inval[0], inval[1]), conn)
    conn.close()
    print("An initial ht df of ", len(ht_df), " rows has been created for offset ", inval[1], ". Now adding additional rows...")
    
    original_length = len(ht_df)
    
    append_rows = np.load(directory + "append_dict_" + str(inval[1]) + ".npy")
    
    append_df = pd.DataFrame()
    for f in append_rows:
        for extra_oclc in f[1]: 
            app_row = f[0].copy()
            app_row[3] = extra_oclc
            append_df = append_df.append(app_row, ignore_index=True)
        if len(append_df)%20000 == 0:
            print("For offset ", inval[1], " current append df is ", len(append_df))
        
        
    ht_df = ht_df.append(append_df, ignore_index=True)

    print("The Hathi DF has increased by ", len(ht_df)-original_length, " rows for offset ", inval[1])

    ## INDEX BUILDING FOR HT_DF RECORDS

    year_ind = {}
    first_ind = {}
    notitle_count = 0

    for rec in ht_df.itertuples():
        if rec[6] == '':
            notitle_count+=1
            continue
        try:
            first_word = re.sub(r'\W+', '', rec[6].split()[0])
        except:
            continue
        try:
            first_ind[first_word].append([rec[6],rec[2]])
        except KeyError:
            first_ind[first_word] = []
            first_ind[first_word].append([rec[6],rec[2]])
        else:
            pass
        yr_ind = None
        try:
            yr_ind = re.findall(r'\d\d\d\d', rec[4])[0]
        except:
            pass
        if yr_ind != None:
            try:
                year_ind[yr_ind]
                try:
                    year_ind[yr_ind][first_word].append([rec[6],rec[2]])
                except:
                    year_ind[yr_ind][first_word] = []
                    year_ind[yr_ind][first_word].append([rec[6],rec[2]])
            except:
                year_ind[yr_ind] = {}
                try:
                    year_ind[yr_ind][first_word].append([rec[6],rec[2]])
                except:
                    year_ind[yr_ind][first_word] = []
                    year_ind[yr_ind][first_word].append([rec[6],rec[2]])
        del yr_ind
        del first_word

    print("A total of ", notitle_count, " records did not have any title in offset ", inval[1])
    print("An index of years with this many records: ",len(year_ind), " was made for offset ", inval[1])
    print("An index of first words with this many records: ",len(first_ind), " was made for offset ", inval[1])
    np.save(directory + "yearIndex_" + str(inval[1]) + ".npy", year_ind)
    np.save(directory + "firstwordIndex_" + str(inval[1]) + ".npy", first_ind)

pool = Pool(processes=5)

rgs = [(4000000,0),(4000000,4000000),(4000000,8000000),(4000000,12000000),(208265,16000000)]

pool.map(ht_indexer,rgs)



end = time.clock()
print("Time elapsed = ", (end-start)/3600)
